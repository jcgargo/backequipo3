package mis.citas.hackaton.modelo;

import org.springframework.data.annotation.Id;

import java.util.ArrayList;
import java.util.List;

public class Cita {

    @Id
    String id;
    String idSucursal;
    String idPerfil;
    String turno;
    String horario;
    String operacion;
    String estatus;

    public Cita(String id, String idSucursal, String idPerfil, String turno, String horario, String operacion, String estatus) {
        this.id = id;
        this.idSucursal = idSucursal;
        this.idPerfil = idPerfil;
        this.turno = turno;
        this.horario = horario;
        this.operacion = operacion;
        this.estatus = estatus;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdSucursal() {
        return idSucursal;
    }

    public void setIdSucursal(String idSucursal) {
        this.idSucursal = idSucursal;
    }

    public String getIdPerfil() {
        return idPerfil;
    }

    public void setIdPerfil(String idPerfil) {
        this.idPerfil = idPerfil;
    }

    public String getTurno() {
        return turno;
    }

    public void setTurno(String turno) {
        this.turno = turno;
    }

    public String getHorario() {
        return horario;
    }

    public void setHorario(String horario) {
        this.horario = horario;
    }

    public String getOperacion() {
        return operacion;
    }

    public void setOperacion(String operacion) {
        this.operacion = operacion;
    }

    public String getEstatus() {
        return estatus;
    }

    public void setEstatus(String estatus) {
        this.estatus = estatus;
    }
}
